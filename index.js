var request = require("request");
var cheerio = require("cheerio");

var comp_name = "#quote-header-info h1";
var last = "#qwidget_lastsale";
var net_change = "#qwidget_netchange";
var net_change_percent = "#qwidget_percent";
var news = "#CompanyNewsCommentary .orange-ordered-list li";
var one_year_target = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(1) > div:nth-child(2)";
var today_high_low = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2)";
var share_volume = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(3) > div:nth-child(2)";
var ninety_day_avg_volume = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(4) > div:nth-child(2)";
var previous_close = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(5) > div:nth-child(2)";
var fiftytow_high_low = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(6) > div:nth-child(2)";
var market_cap = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(7) > div:nth-child(2)";
var pe_ratio = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(2) > div > div:nth-child(8) > div:nth-child(2)";
var forward_pe = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(1) > div:nth-child(2)";
var eps = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(2) > div:nth-child(2)";
var anualized_dividend = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(3) > div:nth-child(2)";
var ex_dividend = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(4) > div:nth-child(2)";
var dividend_date = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(5) > div:nth-child(2)";
var curr_yield = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(6) > div:nth-child(2)";
var beta = "#left-column-div > div.row.overview-results.relativeP > div:nth-child(3) > div > div:nth-child(7) > div:nth-child(2)";

request({
	uri: "https://finance.yahoo.com/quote/msft",
}, function(error, response, body) {
	var $ = cheerio.load(body);
	
	console.log($(comp_name).text().trim());
});

//get stock info
request({
	uri: "https://www.nasdaq.com/symbol/msft",
}, function(error, response, body) {
	var $ = cheerio.load(body);
	
	console.log($(one_year_target).text().trim());
	
	//last price
	console.log($(last).text().substr(1));
	
	//net change
	console.log($(net_change).text());
	
	//net change percentage
	console.log($(net_change_percent).text().slice(0, -1));
	
	//news and link
	$(news).each(function() {
		var head = $(this).find('a');
		var source = $(this).find('small');
		console.log(head.text().trim());
		console.log(head.attr('href'));
		console.log(source.text());
	});
});


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};


//get next earning date 
request({
	uri: "https://www.nasdaq.com/earnings/report/wmt",
}, function(error, response, body) {
	var $ = cheerio.load(body);
	
	//earning
	$("#left-column-div > h2").each(function() {
		console.log($(this).text().split(':').pop().trim());
	});

	
});






